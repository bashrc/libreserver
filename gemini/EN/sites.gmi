=> index.gmi ⯇ Go Back

# Various gemini sites

``` world logo
                  ████▒▒▒▒▒▒
              ▓▓▓▓░░░░░░▒▒▒▒▒▒░░             ▓
          ████░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒         ▓
        ██░░░░░░░░░░░░▓▓▒▒▒▒▒▒░░░░░░▒▒      ▓ ▓
        ██░░░░░░░░░░░░▓▓▒▒▒▒▒▒▒▒░░░░▒▒
      ██░░░░░░░░░░░░▓▓▓▓▓▓▒▒▒▒▒▒░░░░░░▒▒     ▒
      ██▓▓▓▓░░░░░░▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒░░░░▒▒
    ██▓▓▓▓▓▓░░░░▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒░░▒▒▒▒  ▒
    ██▓▓▓▓▓▓▓▓░░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒
    ██▓▓▓▓▓▓▓▓░░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒░░▒▒
    ██▓▓▓▓▓▓▓▓▓░░▓░░░░▓▓▓▓▓▓▓▓▒▒▒▒▒▒░░░░▒▒
    ██▓▓▓▓▓▓▓▓▓▓▓░░░░░░▓▓▓▓▓▓▓▓▓▒▒░░░░░░▒▒
      ██▓▓▓▓▓▓▓▓▓▓░░░░░░░░▓▓▓▓▓▓▒▒▒▒░░▒▒
      ██▓▓▓▓▓▓▓▓░░░░░░░░░░░░▓▓▓▓▒▒▒▒░░▒▒
        ██▓▓▓▓▓▓▓▓░░░░░░░▓▓▓▓▓▓▓▓▓▒▒▒▒
        ██▓▓▓▓▓▓▓▓▓▓░░░░▓▓▓▓▓▓▓▓▓▓▒▒▒▒
          ████▓▓▓▓▓▓░░▓▓▓▓▓▓▓▓▓▓██▒▒
              ████▓▓▓▓▓▓▓▓▓▓████
                  ██████████
```

## LibreServer related sites

=> epicyon/index.gmi Epicyon ActivityPub server

## Other sites

=> gemini://geminiprotocol.net ► The main Gemini project site
=> gemini://warmedal.se/~antenna ► Antenna
=> gemini://cdg.thegonz.net ► Collaborative Directory
=> gemini://skyjake.fi/~Cosmos ► Cosmos
=> gemini://gemi.dev/cgi-bin/wp.cgi ► Gemipedia
=> gemini://konpeito.media ► Konpeito Media
